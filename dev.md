This file contains information for people looking to understand or modify the code for this application.

# Basics

This is a Kotlin project, evolving out of the shell of a Java one.

# Layers

The **data** layer contains classes corresponding to raw database records.

The **model** layer contains higher-level representations of things.


# Concepts

Each song can have up to 1 file.
The files are separated in a way that should make it easy to add support for multiple files in the future.

Each label can apply to any number of songs, and each song can have any number of labels.
Labels have a parent-child or "implies" relationship: the parent implies the child.
(E.g. if SmoothJazz is a child of Jazz, searches for Jazz will also return songs marked SmoothJazz.)

However, a label can be marked as an **exclusive set**, in which case each song can only have 1 of the children applied.
(So if Ratings is an exclusive set and the parent of both Good and Bad, a song cannot be both Good and Bad.)

A label that is an exclusive set can also be marked **sorted**.
This means that you pick what order the child labels are in, and can search based on the ordering.
(So if Ratings is a sorted exclusive set and the parent of Bad, Mediocre, and Good, in that order,
then you can run searches like "Rating > Bad" to return all songs that are Mediocre or Good.)