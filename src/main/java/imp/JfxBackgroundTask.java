package imp;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**Maintains a queue of tasks that are executed in a background thread.
 * May only be interacted with from the UI thread. */
/*Implementation note: member variables must only be modified on the UI thread. (Except the queue.)*/
public class JfxBackgroundTask//TODO move to util project
{
	private final String threadName;
	private final Queue<Action> queue = new ConcurrentLinkedQueue<>();
	private final Logger log = LogManager.getLogger(getClass());

	private final ReadOnlyIntegerWrapper unfinishedInternal = new ReadOnlyIntegerWrapper(0);
	/**How many submitted tasks haven't been run yet. If 0, all tasks are finished and our background thread is not running.*/
	public final ReadOnlyIntegerProperty unfinishedCount = unfinishedInternal.getReadOnlyProperty();


	public JfxBackgroundTask(String threadName)
	{
		this.threadName = threadName;
	}


	/**Submits a task to be executed later. Must be called from the UI thread.
	 * Don't submit the same action instance multiple times.*/
	public void submitFromUi(Action action)
	{
		queue.add(action);
		unfinishedInternal.set(unfinishedInternal.get() + 1);
		if(unfinishedInternal.get() == 1)
			launchBackgroundThreadFromUi();
	}


	/**Must be called from the UI thread.*/
	private void launchBackgroundThreadFromUi()
	{
		new Thread(this::backgroundRun, threadName).start();
	}



	private void backgroundRun()
	{
		while(true)
		{
			Action action = queue.poll();
			log.debug("Processing next action: {}", action);
			if(action == null)
			{
				Platform.runLater(this::onBackgroundStoppedUi);
				return;
			}

			try
			{
				action.execute();
				Platform.runLater(() -> onTaskFinishedUi(action, null));
			} catch(Exception e)
			{
				log.warn("Encountered error running {}", action, e);
				Platform.runLater(() -> onTaskFinishedUi(action, e));
			}
		}
	}



	/**Runs in the UI thread with a task that finished executing in the background.
	 * @param error the Exception execute() threw, if any*/
	private void onTaskFinishedUi(Action action, @Nullable Exception error)
	{
		log.debug("Post-execute for {} with error {}", action, error);
		unfinishedInternal.set(unfinishedInternal.get() - 1);
		if(error == null)
			action.postExecute();
		else
			action.postExecuteFailure(error);
	}


	/**Runs in the UI thread when we run out of tasks for the background thread to process.*/
	private void onBackgroundStoppedUi()
	{
		if(!queue.isEmpty())
			launchBackgroundThreadFromUi();
	}



	/**Something that runs in a background thread.
	 * To carry data from execute() to postExecute(), store it in an instance variable of your Action subclass.
	 * Consider overriding toString() to make logging more useful.*/
	public interface Action
	{
		/**Does something in a background thread.*/
		void execute() throws Exception;
		/**Override to do something in the UI thread after execute() has finished.*/
		default void postExecute(){}
		/**Override to do something in the UI thread after execute() has thrown an Exception.*/
		@SuppressWarnings("UseOfSystemOutOrSystemErr") default void postExecuteFailure(Exception e){throw new RuntimeException(e);}
	}
}
