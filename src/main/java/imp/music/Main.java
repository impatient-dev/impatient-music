package imp.music;

import imp.music.storage.Storage;
import imp.music.ui.MainUi;
import imp.orm.Orm;
import javafx.application.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main
{
	private static final Logger log = LogManager.getLogger(Main.class);

	public static void main(String[] args)
	{
		log.info("Starting up.");
		Orm orm = Storage.orm();
		orm.validate();
		Storage.setupIfNecessary();
		Application.launch(MainUi.class, args);
	}
}
