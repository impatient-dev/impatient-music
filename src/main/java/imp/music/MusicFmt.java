package imp.music;

import javax.annotation.Nullable;
import java.util.concurrent.TimeUnit;

public class MusicFmt
{
	/**Returns something like 01:23.*/
	public static String seconds(long seconds)
	{
		StringBuilder out = new StringBuilder();

		long hours = TimeUnit.SECONDS.toHours(seconds);
		if(hours > 0)
		{
			out.append(hours);
			out.append(':');
			seconds -= TimeUnit.HOURS.toSeconds(hours);
		}

		long minutes = TimeUnit.SECONDS.toMinutes(seconds);
		out.append(minutes);
		out.append(':');
		seconds -= TimeUnit.MINUTES.toSeconds(minutes);

		if(seconds < 10)
			out.append('0');
		out.append(seconds);
		return out.toString();
	}


	public static String fxDuration(@Nullable javafx.util.Duration duration)
	{
		if(duration == null)
			return "";
		double seconds = duration.toSeconds();
		return Double.isNaN(seconds) ? "" : seconds((long)seconds);
	}
}
