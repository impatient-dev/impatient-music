package imp.music.trans;

import imp.music.data.Song;
import imp.music.data.SongFile;
import imp.music.model.SongModel;
import imp.music.storage.Storage;
import imp.orm.Orm;
import imp.orm.Query;
import imp.sqlite.Database;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/** Describes a search for songs.
 * Currently over-engineered, to allow for more-fancy searches later.*/
public class Search
{
	private final Orm orm = Storage.orm();
	@Nullable private final String matchText;

	private Search(@Nullable String matchText)
	{
		this.matchText = matchText;
	}


	public static Search parseUserSearch(String str)
	{
		String matchText = str.isEmpty() ? null : str;
		return new Search(matchText);
	}


	@Override public String toString(){return String.format("Search[%s]", matchText == null ? "" : matchText);}


	public List<SongModel> find(Database db)
	{
		Query<Song> query = orm.createQuery(Song.class);
		List<SongModel> out = new ArrayList<>();
		String m = matchText == null ? null : matchText.toLowerCase();

		for(Song song : query.execute(db))
		{
			if(matchText == null || matchLower(m, song.name) || matchLower(m, song.artist) || matchLower(m, song.mix))
			{
				List<SongFile> files = orm.createQuery(SongFile.class).andWhereEquals("song", song.id).execute(db);
				out.add(SongModel.from(song, files));
			}
		}

		return out;
	}


	private static boolean matchLower(String queryLowercase, String songValue)
	{
		return songValue.toLowerCase().contains(queryLowercase);
	}
}
