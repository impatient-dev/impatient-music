package imp.music.trans;

import imp.music.model.LabelModel;
import imp.music.storage.Labels;

import java.util.List;
import java.util.stream.Collectors;

//TODO make this an object with internal data, like the song version
public class LabelSearch
{
	/**Returns all labels that match the user's search.*/
	public static List<LabelModel> find(String search, Labels labels)
	{
		return labels.labels().stream()
				.filter(label -> search.isEmpty() || label.name.contains(search))
				.collect(Collectors.toList());
	}
}
