package imp.music.ui;

import imp.music.storage.Storage;
import imp.sqlite.Database;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**The top-level application window.*/
public class MainUi extends Application
{
	private final MainMenu vMenu;
	private final SongsListPanel vSongs;
	private final SelectionPanel vSelect;
	private final PlayingPanel vPlay;


	public MainUi()
	{
		try(Database db = Storage.connect())
		{
			WindowState state = new WindowState(db, this::onSelectSongId, this::onSelectLabelId);
			vMenu = new MainMenu(state);
			vSongs = new SongsListPanel(state);
			vSelect = new SelectionPanel(state);
			vPlay = new PlayingPanel(state);
		}
	}


	@Override public void start(Stage stage)
	{
		BorderPane layout = new BorderPane(vSelect.root(), vMenu.root(), null, vPlay.root(), vSongs.root());
		Scene scene = new Scene(layout, 1000, 500);
//		scene.getStylesheets().add("style.css");//TODO
		stage.setScene(scene);
		stage.setTitle("Impatient Music");
		stage.show();
	}


	private void onSelectSongId(long id){vSelect.selectSong(id);}
	private void onSelectLabelId(long id){vSelect.selectLabel(id);}
}
