package imp.music.ui;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**Manages the main window's top menu.*/
public class MainMenu
{
	private final MenuItem itemNewSong = new MenuItem("New Song"), itemNewLabel = new MenuItem("New Label");
	{
		//TODO handlers
	}

	private final Menu menu = new Menu("Menu", null, itemNewSong, itemNewLabel);
	private final MenuBar root = new MenuBar(menu);


	public MainMenu(WindowState state)
	{
		itemNewSong.setOnAction(__ -> state.onSelectSongId.accept(-1L));
		itemNewLabel.setDisable(true);
	}


	public MenuBar root(){return root;}
}
