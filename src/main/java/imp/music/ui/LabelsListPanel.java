package imp.music.ui;

import imp.music.model.LabelModel;
import imp.music.storage.Storage;
import imp.music.trans.LabelSearch;
import imp.music.ui.list.LabelSearchPanel;
import imp.sqlite.Database;
import impatience.JfxUtil;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Shows a list of selectable songs. */
public class LabelsListPanel
{
	private final WindowState state;
	private final Logger log = LogManager.getLogger(getClass());

	private String search;

	private final LabelSearchPanel vSearch = new LabelSearchPanel(this::setSearch);

	private final Node vEmpty = new Label("No Songs Found");
	private final ListView<LabelModel> vList = new ListView<>();
	private final StackPane vResults = new StackPane();

	private final Node root = new VBox(vSearch.root(), vResults);


	public LabelsListPanel(WindowState state)
	{
		this.state = state;
		state.lastChange.addListener((a,b,c) -> refresh());
		vList.setCellFactory(__ -> new MyRow());
		setSearch(vSearch.getSearch());
	}


	public Node root(){return root;}


	private void setSearch(String search)
	{
		log.debug("Changing label search to {}", search);
		this.search = search;
		refresh();
	}


	private void refresh()
	{
		try(Database db = Storage.connect())
		{
			vList.setItems(FXCollections.observableList(LabelSearch.find(search, state.labels)));//TODO not in UI thread
		}
		JfxUtil.replaceChild(vResults, vList.getItems().isEmpty() ? vEmpty : vList);
	}



	private class MyRow extends ListCell<LabelModel>
	{
		@Override protected void updateItem(LabelModel label, boolean empty)
		{
			super.updateItem(label, empty);
			setGraphic(null);
			if(empty)
				return;
			setText(label.name);
			setOnMouseClicked(this::onClick);
		}

		private void onClick(Object event){state.onSelectLabelId.accept(getItem().id);}
	}
}
