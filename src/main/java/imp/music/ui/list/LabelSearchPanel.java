package imp.music.ui.list;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.util.function.Consumer;

/** Describes search criteria for labels. */
public class LabelSearchPanel
{
	private final Consumer<String> onSearch;

	private final TextField vField = new TextField();
	private final Button vBtn = new Button("Search");
	private final Node root = new HBox(vField, vBtn);


	public LabelSearchPanel(Consumer<String> onSearch)
	{
		this.onSearch = onSearch;
		vBtn.setOnAction(__ -> doSearch());
		vField.setOnAction(__ -> doSearch());
	}


	public Node root(){return root;}


	private void doSearch()
	{
		onSearch.accept(getSearch());
	}

	public String getSearch()
	{
		return vField.getText();
	}
}
