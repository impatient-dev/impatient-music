package imp.music.ui.sub;

import imp.music.trans.Search;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.util.function.Consumer;

/** Describes search criteria for songs. */
public class SongSearchPanel
{
	private final Consumer<Search> onSearch;

	private final TextField vField = new TextField();
	private final Button vBtn = new Button("Search");
	private final Node root = new HBox(vField, vBtn);


	public SongSearchPanel(Consumer<Search> onSearch)
	{
		this.onSearch = onSearch;
		vBtn.setOnAction(__ -> doSearch());
		vField.setOnAction(__ -> doSearch());
	}


	public Node root(){return root;}


	private void doSearch()
	{
		onSearch.accept(getSearch());
	}

	public Search getSearch()
	{
		return Search.parseUserSearch(vField.getText());
	}
}
