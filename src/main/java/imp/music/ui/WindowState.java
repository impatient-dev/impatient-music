package imp.music.ui;

import imp.music.storage.Labels;
import imp.orm.Orm;
import imp.sqlite.Database;
import javafx.beans.property.LongProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.Instant;
import java.util.function.Consumer;

/** Global state used by different parts of the window.
 * Listeners should be registered when components are created, and kept around forever; no need to bother with WeakListeners. */
public class WindowState
{
	public final Labels labels;
	public final LongProperty playingFileId = new SimpleLongProperty(Orm.INVALID_KEY);

	/**-1 means we want to "select" and start editing a new song that hasn't been created yet.*/
	public final Consumer<Long> onSelectSongId;
	/**-1 means we want to "select" and start editing a new label that hasn't been created yet.*/
	public final Consumer<Long> onSelectLabelId;


	/**The last time anything was edited in the database.
	 * Add listeners to this to refresh when someone else edits something.
	 * Change this whenever you modify something in the database.*/
	public final Property<Instant> lastChange = new SimpleObjectProperty<>(Instant.now());


	public WindowState(Database db, Consumer<Long> onSelectSongId, Consumer<Long> onSelectLabelId)
	{
		this.labels = new Labels(db);
		this.onSelectSongId = onSelectSongId;
		this.onSelectLabelId = onSelectLabelId;
	}
}
