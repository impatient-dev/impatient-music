package imp.music.ui.select;

import imp.music.model.LabelModel;
import imp.music.storage.Storage;
import imp.music.ui.SelectionPanel;
import imp.music.ui.WindowState;
import imp.orm.Orm;
import imp.sqlite.Database;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**Lets the user edit an existing label or create a new one.*/
public class LabelEditPanel implements SelectionPanel.MyTab //TODO fix copy-paste job
{
	private final WindowState state;
	private LabelModel label;
	private final StringProperty tabName = new SimpleStringProperty();
	private final BooleanProperty unsavedChanges = new SimpleBooleanProperty(false);


	private final Label vTitle = new Label();
	private final Label vSave = new Label();
	private final Node vTitleRow = new HBox(vTitle, vSave);

	private final Label vId = new Label();
	private final TextField vName = new TextField(), vDescription = new TextField();
	private final GridPane vEditArea = new GridPane();
	{
		int r = 0;
		vEditArea.addRow(r++, vId);
		vEditArea.addRow(r++, new Label("Name"), vName);
		vEditArea.addRow(r++, new Label("Description"), vDescription);
	}

	private final Node root = new VBox(vTitleRow, vEditArea);


	public LabelEditPanel(WindowState state, long labelId)
	{
		this.state = state;
		label = labelId == -1 ? LabelModel.newUnsaved() : state.labels.getLabel(labelId);
		refreshEditArea();

		for(TextField field : new TextField[]{vName, vDescription})
		{
			field.textProperty().addListener((a, b, c) -> unsavedChanges.set(true));
			field.setOnAction(__ -> save());
		}

		unsavedChanges.addListener((a,b,c) -> refreshSaving());
	}



	@Override public Node root(){return root;}
	@Override public StringProperty tabName(){return tabName;}
	public long labelId(){return label.id;}


	private void refreshSaving()
	{
		String msg = label.id == Orm.INVALID_KEY ? "new" : "edit";
		if(unsavedChanges.get())
			msg += " (unsaved)";
		vSave.setText(msg);
	}


	private void refreshEditArea()
	{
		vTitle.setText("Edit");
		vId.setText(String.valueOf(label.id));
		vName.setText(label.name);
		vDescription.setText(label.description);
		tabName.setValue(label.name);
	}


	private void save()
	{
		try(Database db = Storage.connect())
		{
			state.labels.update(label, vName.getText(), vDescription.getText(), db);
		}
	}
}
