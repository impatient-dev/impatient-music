package imp.music.ui.select;

import imp.JfxBackgroundTask;
import imp.music.data.Song;
import imp.music.data.SongFile;
import imp.music.model.SongModel;
import imp.music.storage.Storage;
import imp.music.ui.SelectionPanel;
import imp.music.ui.WindowState;
import imp.orm.Orm;
import imp.sqlite.Database;
import impatience.CollectionUtil;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javax.annotation.Nullable;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;

/**Lets the user edit an existing song or create a new one.*/
public class SongEditPanel implements SelectionPanel.MyTab
{
	private final WindowState state;
	private SongModel song;
	private final JfxBackgroundTask background = new JfxBackgroundTask("SongEditPanel");
	private final StringProperty tabName = new SimpleStringProperty();
	private final BooleanProperty unsavedChanges = new SimpleBooleanProperty(false);
	private final SimpleBooleanProperty inputDisabled = new SimpleBooleanProperty();
	{inputDisabled.bind(background.unfinishedCount.greaterThan(0));}


	private final Label vTitle = new Label();
	private final Label vSave = new Label();
	private final Node vTitleRow = new HBox(vTitle, vSave);

	private final Label vId = new Label();
	private final TextField vName = new TextField(), vArtist = new TextField(), vMix = new TextField(), vDescription = new TextField();
	private final SongFilesPanel vFiles = new SongFilesPanel(new FilesComponentListener(), inputDisabled);
	private final GridPane vEditArea = new GridPane();
	{
		int r = 0;
		vEditArea.addRow(r++, vId);
		vEditArea.addRow(r++, new Label("Song"), vName);
		vEditArea.addRow(r++, new Label("Artist"), vArtist);
		vEditArea.addRow(r++, new Label("Mix"), vMix);
		vEditArea.addRow(r++, new Label("Description"), vDescription);
		vEditArea.addRow(r++, vFiles.root());
	}

	private final Node root = new VBox(vTitleRow, vEditArea);


	public SongEditPanel(WindowState state, long songId)
	{
		this.state = state;
		song = SongModel.newUnsaved();
		refreshEditArea();
		if(songId != -1)
			background.submitFromUi(new LoadAction(songId));

		for(TextField field : new TextField[]{vName, vArtist, vMix, vDescription})
		{
			field.textProperty().addListener((a, b, c) -> unsavedChanges.set(true));
			field.setOnAction(__ -> background.submitFromUi(new SaveAction(null, null)));
		}

		for(Node n : new Node[]{vName, vArtist, vMix, vDescription})
			n.disableProperty().bind(inputDisabled);
		background.unfinishedCount.addListener((a,b,c) -> refreshSaving());
		unsavedChanges.addListener((a,b,c) -> refreshSaving());
	}



	@Override public Node root(){return root;}
	@Override public StringProperty tabName(){return tabName;}
	public long songId(){return song.id;}


	private void refreshSaving()
	{
		String msg;
		if(background.unfinishedCount.get() > 0)
			msg = "saving / loading";
		else
		{
			msg = song.id == Orm.INVALID_KEY ? "new" : "edit";
			if(unsavedChanges.get())
				msg += " (unsaved)";
		}
		vSave.setText(msg);
	}


	private void refreshEditArea()
	{
		vTitle.setText("Edit");
		vId.setText(String.valueOf(song.id));
		vName.setText(song.name);
		vArtist.setText(song.artist);
		vMix.setText(song.mix);
		vDescription.setText(song.description);
		vFiles.setFiles(song.files);
		tabName.setValue(song.name);//TODO should rename method if this is included
	}




	/**Saves a new or preexisting song. Will always make us reload when finished.*/
	private class SaveAction implements JfxBackgroundTask.Action
	{
		private final SongModel model = song.copy();
		@Nullable private final SongFile fileToDelete;
		@Nullable private final Path fileToAttach;
		private long savedSongId;


		public SaveAction(@Nullable SongFile delete, @Nullable Path attach)
		{
			this.fileToDelete = delete;
			this.fileToAttach = attach;
			unsavedChanges.set(false);

			model.name = vName.getText();
			model.artist = vArtist.getText();
			model.mix = vMix.getText();
			model.description = vDescription.getText();
		}

		@Override public String toString(){return String.format("Save[song=%s attachFile=%s deleteFile=%s]", model.id, fileToAttach, fileToDelete);}


		@Override public void execute() throws Exception
		{
			Song song = new Song(model.id, model.name, model.artist, model.mix, model.description);

			try(Database db = Storage.connect())
			{
				Orm orm = Storage.orm();
				db.beginTransaction();

				orm.save(song, db);
				savedSongId = song.id;
				if(fileToDelete != null)
					orm.delete(fileToDelete, db);
				if(fileToAttach != null)
					SongFile.importFile(fileToAttach, song, db, orm);

				db.commit();
			}
		}


		@Override public void postExecute()
		{
			state.lastChange.setValue(Instant.now());
			background.submitFromUi(new LoadAction(savedSongId));
		}
	}



	/**Loads a song by ID.*/
	private class LoadAction implements JfxBackgroundTask.Action
	{
		private final long songId;
		private SongModel newModel;

		public LoadAction(long songId){this.songId = songId;}

		@Override public String toString(){return String.format("Load[%s]", songId);}


		@Override public void execute()
		{
			try(Database db = Storage.connect())
			{
				Orm orm = Storage.orm();
				Song song = CollectionUtil.getOnly(orm.createQuery(Song.class).andWhereEquals("id", songId).execute(db));
				List<SongFile> files = orm.createQuery(SongFile.class).andWhereEquals("song", songId).execute(db);
				newModel = SongModel.from(song, files);
			}
		}


		@Override public void postExecute()
		{
			SongEditPanel.this.song = newModel;
			refreshEditArea();
			unsavedChanges.set(false);
		}
	}



	private class FilesComponentListener implements SongFilesPanel.Listener
	{
		@Override public void addFile(Path file)
		{
			if(song.id == Orm.INVALID_KEY)
				throw new UnsupportedOperationException("Refusing to attach because we don't have a song ID.");//TODO
			background.submitFromUi(new SaveAction(null, file));
			background.submitFromUi(new LoadAction(song.id));
		}

		@Override public void removeFile(SongFile file)
		{
			background.submitFromUi(new SaveAction(file, null));
			background.submitFromUi(new LoadAction(song.id));
		}
	}
}
