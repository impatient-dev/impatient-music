package imp.music.ui.select;

import imp.music.data.SongFile;
import impatience.CollectionUtil;
import impatience.JfxUtil;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;

/** Shows the files attached to a song. */
public class SongFilesPanel
{
	private final ObservableList<SongFile> files = FXCollections.observableArrayList();
	private final Listener listener;
	private final ReadOnlyBooleanProperty inputDisabled;

	private final Node vEmpty = new Label("No Files Found");
	private final ListView<SongFile> vList = new ListView<>();
	private final StackPane root = new StackPane();


	/**When input is disabled, the user cannot make changes.*/
	public SongFilesPanel(Listener listener, ReadOnlyBooleanProperty inputDisabled)
	{
		this.listener = listener;
		this.inputDisabled = inputDisabled;
		vList.setCellFactory(__ -> new MyRow());
		vList.setItems(files);

		root.setOnDragEntered(this::onDragEnter);
		root.setOnDragExited(this::onDragExit);
		root.setOnDragOver(this::onDragOver);
		root.setOnDragDropped(this::onDragDrop);
		Tooltip.install(root, new Tooltip("Drag a file to attach"));

		setFiles(Collections.emptyList());
	}


	public Node root(){return root;}



	public void setFiles(Collection<SongFile> newFileList)
	{
		files.setAll(newFileList);
		JfxUtil.replaceChild(root, files.isEmpty() ? vEmpty : vList);
	}


	private void onDragEnter(DragEvent event){System.err.println("DRAG ENTER");/*TODO DEBUG*/}
	private void onDragExit(DragEvent event){System.err.println("DRAG EXIT");/*TODO DEBUG*/}
	private void onDragOver(DragEvent event)
	{
		if(event.getDragboard().hasFiles())
			event.acceptTransferModes(TransferMode.COPY);
	}

	private void onDragDrop(DragEvent event)//TODO hilight when dragging happens
	{
		System.err.println("DRAG DROP");//TODO DEBUG
		File file = CollectionUtil.getOnly(event.getDragboard().getFiles());//TODO error handling
		listener.addFile(file.toPath());//TODO error handling
		event.setDropCompleted(true);
	}



	private class MyRow extends ListCell<SongFile>
	{
		private final Label vName = new Label();
		private final Button vDelete = new Button("Delete");
		private final Node root = new HBox(vName, vDelete);


		public MyRow()
		{
			vDelete.disableProperty().bind(inputDisabled);
		}


		@Override protected void updateItem(SongFile file, boolean empty)
		{
			super.updateItem(file, empty);
			if(empty)
				return;
			vName.setText(getItem().toString());
			setGraphic(root);
		}

	}


	public interface Listener
	{
		void addFile(Path file);
		void removeFile(SongFile file);
	}
}
