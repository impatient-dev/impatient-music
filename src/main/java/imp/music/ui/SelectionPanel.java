package imp.music.ui;

import imp.music.ui.select.LabelEditPanel;
import imp.music.ui.select.SongEditPanel;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

/** Shows selected songs/labels, and lets the user edit them.*/
public class SelectionPanel
{
	private final Logger log = LogManager.getLogger(getClass());
	private final WindowState state;
	private final Map<Tab, MyTab> myTabs = new HashMap<>();

	private final TabPane vTabs = new TabPane();
	private final Node root = vTabs;


	public SelectionPanel(WindowState state)
	{
		this.state = state;
		vTabs.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
	}


	public Node root(){return root;}

	private void addAndSelectTab(MyTab tab)
	{
		Tab vTab = new Tab();
		log.trace("adding tab {} as {}", tab, vTab);
		vTab.textProperty().bind(tab.tabName());
		vTab.setContent(tab.root());
		myTabs.put(vTab, tab);
		vTab.setOnClosed(__ -> myTabs.remove(vTab));
		vTabs.getTabs().add(vTab);
		vTabs.getSelectionModel().select(vTab);
	}


	/**Selects the first tab that matches the condition. If no tabs match the condition, adds the tab from the creator and selects that.*/
	private void selectTab(Predicate<MyTab> condition, Supplier<MyTab> creator)
	{
		for(Map.Entry<Tab, MyTab> entry : myTabs.entrySet())
		{
			if(condition.test(entry.getValue()))
			{
				log.trace("Selecting existing tab {}", entry.getValue());
				vTabs.getSelectionModel().select(entry.getKey());
				return;
			}
		}

		addAndSelectTab(creator.get());
	}

	public void selectSong(long id)
	{
		log.debug("Selecting song {}", id);
		selectTab(t -> id != -1 && t instanceof SongEditPanel && ((SongEditPanel)t).songId() == id, () -> new SongEditPanel(state, id));
	}


	public void selectLabel(long id)
	{
		log.debug("Selecting label {}", id);
		selectTab(t -> id != -1 && t instanceof LabelEditPanel && ((LabelEditPanel)t).labelId() == id, () -> new LabelEditPanel(state, id));
	}


	public interface MyTab
	{
		StringProperty tabName();
		Node root();
	}
}
