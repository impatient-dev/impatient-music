package imp.music.ui;

import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

/** Lets the user view lists of songs and labels. */
public class ListPanel
{
	private final WindowState state;

	private final SongsListPanel vSongs;
	private final LabelsListPanel vLabels;
	private final TabPane vTabs;

	public ListPanel(WindowState state)
	{
		this.state = state;
		vSongs = new SongsListPanel(state);
		vLabels = new LabelsListPanel(state);
		vTabs = new TabPane(new Tab("Songs", vSongs.root()), new Tab("Labels", vLabels.root()));
	}


	public Node root(){return vTabs;}
}
