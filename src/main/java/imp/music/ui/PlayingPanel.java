package imp.music.ui;

import imp.JfxBackgroundTask;
import imp.music.MusicFmt;
import imp.music.data.Song;
import imp.music.data.SongFile;
import imp.music.storage.Storage;
import imp.orm.Orm;
import imp.sqlite.Database;
import impatience.JfxUtil;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.nio.file.Path;

/** Shows what song is playing, and lets the user pause etc. */
public class PlayingPanel
{
	private final WindowState state;
	private final SimpleObjectProperty<Song> song = new SimpleObjectProperty<>(null);
	private final SimpleObjectProperty<SongFile> file = new SimpleObjectProperty<>(null);
	@Nullable private MediaPlayer mediaPlayer;
	private final Logger log = LogManager.getLogger(getClass());

	private final JfxBackgroundTask background = new JfxBackgroundTask("PlayingPanel");
	private final BooleanProperty inputDisabled = new SimpleBooleanProperty();
	{inputDisabled.bind(background.unfinishedCount.greaterThan(0).or(Bindings.isNull(file)));}


	private final ProgressBar vProgress = new ProgressBar();
	private final Label vCurrentTime = new Label(), vTotalTime = new Label(), vTimeSeparator = new Label(" / ");
	private final Node vProgressRow = new HBox(vProgress, vCurrentTime, vTimeSeparator, vTotalTime);

	private final Label vName = new Label();

	private final Button vPlay = new Button("Play"), vPause = new Button("Pause"), vStop = new Button("Stop");
	private final Node vControls = new HBox(vPlay, vPause, vStop);

	private final Node root = new VBox(vProgressRow, vName, vControls);


	public PlayingPanel(WindowState state)
	{
		this.state = state;
		state.playingFileId.addListener((a,b,c) -> setPlayingFileId(state.playingFileId.get()));

		vProgress.setProgress(getProgress());
		vName.textProperty().bind(Bindings.createStringBinding(() -> song.getValue() == null ? "" : song.getValue().toFriendlyString(), song));

		for(Node n : new Node[]{vPlay, vPause, vStop})
			n.disableProperty().bind(inputDisabled);
		vPlay.setOnAction(this::onPlay);
		vPause.setOnAction(this::onPause);
		vStop.setOnAction(this::onStop);
	}



	public Node root(){return root;}


	private void setPlayingFileId(long id)
	{
		log.info("Play song file {}", id);
		if(id == Orm.INVALID_KEY)
		{
			song.setValue(null);
			file.set(null);
			mediaPlayer = null;
		}
		else
			background.submitFromUi(new LoadAndUseTask(id));
	}


	private void onPlay(Object __)
	{
		mediaPlayer.play();
	}
	private void onPause(Object __)
	{
		mediaPlayer.pause();
	}
	private void onStop(Object __)
	{
		mediaPlayer.stop();
	}


	private class LoadAndUseTask implements JfxBackgroundTask.Action
	{
		private final long songFileId;
		private Song loadedSong;
		private SongFile loadedFile;
		private Path loadedFilePath;

		public LoadAndUseTask(long songFileId){this.songFileId = songFileId;}

		@Override public String toString(){return String.format("LoadAndUse[file=%s]", songFileId);}

		@Override public void execute() throws Exception
		{
			try(Database db = Storage.connect())
			{
				Orm orm = Storage.orm();
				loadedFile = orm.createQuery(SongFile.class)
						.andWhereEquals("id", songFileId)
						.executeFirst(db);
				loadedSong = orm.createQuery(Song.class)
						.andWhereEquals("id", loadedFile.song)
						.executeFirst(db);
			}
			loadedFilePath = Storage.audioFile(loadedFile.id, loadedFile.fileExtension);
		}


		@Override public void postExecute()
		{
			if(mediaPlayer != null)
				onStop(null);

			String uri = loadedFilePath.toUri().toString();
			log.trace("Creating media player for URI " + uri);
			mediaPlayer = new MediaPlayer(new Media(uri));
			file.set(loadedFile);
			song.set(loadedSong);
			JfxUtil.bind(vTotalTime.textProperty(), mediaPlayer.cycleDurationProperty(), MusicFmt::fxDuration);
			JfxUtil.bind(vCurrentTime.textProperty(), mediaPlayer.currentTimeProperty(), MusicFmt::fxDuration);
			vProgress.progressProperty().bind(Bindings.createDoubleBinding(PlayingPanel.this::getProgress, mediaPlayer.currentTimeProperty(), mediaPlayer.cycleDurationProperty()));
			onPlay(null);
		}
	}


	private double getProgress()
	{
		if(mediaPlayer == null)
			return 0;
		javafx.util.Duration current = mediaPlayer.getCurrentTime(), total = mediaPlayer.getCycleDuration();
		if(current == null || current.isIndefinite() || total == null || total.isIndefinite())
			return 0;
		return current.toSeconds() / total.toSeconds();

	}
}
