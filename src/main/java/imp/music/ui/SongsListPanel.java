package imp.music.ui;

import imp.music.model.SongModel;
import imp.music.storage.Storage;
import imp.music.trans.Search;
import imp.music.ui.sub.SongSearchPanel;
import imp.sqlite.Database;
import impatience.JfxUtil;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Shows a list of selectable songs. */
public class SongsListPanel
{
	private final WindowState state;
	private final Logger log = LogManager.getLogger(getClass());

	private Search search;

	private final SongSearchPanel vSearch = new SongSearchPanel(this::setSearch);

	private final Node vEmpty = new Label("No Songs Found");
	private final ListView<SongModel> vList = new ListView<>();
	private final StackPane vResults = new StackPane();

	private final Node root = new VBox(vSearch.root(), vResults);


	public SongsListPanel(WindowState state)
	{
		this.state = state;
		state.lastChange.addListener((a,b,c) -> refresh());
		vList.setCellFactory(__ -> new MyRow());
		setSearch(vSearch.getSearch());
	}


	public Node root(){return root;}


	private void setSearch(Search search)
	{
		log.debug("Changing search to {}", search);
		this.search = search;
		refresh();
	}


	private void refresh()
	{
		try(Database db = Storage.connect())
		{
			vList.setItems(FXCollections.observableList(search.find(db)));//TODO not in UI thread
		}
		JfxUtil.replaceChild(vResults, vList.getItems().isEmpty() ? vEmpty : vList);
	}



	private class MyRow extends ListCell<SongModel>
	{
		private final Label vText = new Label();
		private final Button vPlay = new Button("Play");
		private final Node root = new VBox(vText, vPlay);

		{
			vPlay.setOnAction(this::onPlay);
			root.setOnMouseClicked(this::onClick);
		}

		@Override protected void updateItem(SongModel song, boolean empty)
		{
			super.updateItem(song, empty);
			setGraphic(null);
			if(empty)
				return;
			vText.setText(String.format("%s -- %s (%s)", song.name, song.artist, song.mix));
			vPlay.setDisable(song.files.isEmpty());
			setGraphic(root);
		}

		private void onClick(Object event){state.onSelectSongId.accept(getItem().id);}

		private void onPlay(Object event)
		{
			log.debug("User wants to play {}", getItem());
			if(getItem().files.size() != 1)
				throw new UnsupportedOperationException("Unsure which file to play in a list of " + getItem().files.size());
			state.playingFileId.set(getItem().files.get(0).id);
		}
	}
}
