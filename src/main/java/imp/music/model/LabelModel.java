package imp.music.model;

import imp.music.data.SongLabel;
import imp.orm.Orm;

import java.util.HashSet;
import java.util.Set;

public class LabelModel
{
	public long id;
	public String name, description;
	public final Set<LabelModel> parents = new HashSet<>(), children = new HashSet<>();


	private LabelModel(long id, String name, String description)
	{
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public static LabelModel from(SongLabel record){return new LabelModel(record.id, record.name, record.description);}

	public static LabelModel newUnsaved(){return new LabelModel(Orm.INVALID_KEY, "", "");}

	@Override public String toString(){return String.format("Label[%s]", name);}
	@Override public boolean equals(Object obj){return obj instanceof LabelModel && id == ((LabelModel)obj).id;}
	@Override public int hashCode(){return Long.hashCode(id);}
}
