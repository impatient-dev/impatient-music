package imp.music.model;

import imp.music.data.Song;
import imp.music.data.SongFile;
import imp.orm.Orm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SongModel
{
	public long id = Orm.INVALID_KEY;
	public String name, artist, mix, description;
	public final List<SongFile> files;


	private SongModel(long id, String name, String artist, String mix, String description, List<SongFile> files)
	{
		this.id = id;
		this.name = name;
		this.artist = artist;
		this.mix = mix;
		this.description = description;
		this.files = files;
	}


	public static SongModel newUnsaved()
	{
		return new SongModel(Orm.INVALID_KEY, "", "", "", "", new ArrayList<>());
	}

	public static SongModel from(Song song, List<SongFile> files)
	{
		return new SongModel(song.id, song.name, song.artist, song.mix, song.description, files);
	}

	public SongModel copy()
	{
		List<SongFile> newFiles = files.stream().map(SongFile::copy).collect(Collectors.toCollection(ArrayList::new));
		return new SongModel(id, name, artist, mix, description, newFiles);
	}

	@Override public String toString()
	{
		return String.format("SongModel[%s '%s' -- '%s' (%s)]", id, name, artist, mix);
	}
}
