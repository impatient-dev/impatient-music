package imp.music.model;

import java.util.ArrayList;
import java.util.List;

public class SetModel
{
	public long id;
	public String name, description;
	public final List<LabelModel> labels = new ArrayList<>();

	public SetModel(long id, String name, String description)
	{
		this.id = id;
		this.name = name;
		this.description = description;
	}
}
