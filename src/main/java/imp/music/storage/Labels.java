package imp.music.storage;

import imp.music.data.LabelComparableSet;
import imp.music.data.SongLabel;
import imp.music.data.relations.Label2Label;
import imp.music.data.relations.Label2Set;
import imp.music.model.LabelModel;
import imp.music.model.SetModel;
import imp.orm.Orm;
import imp.sqlite.Database;

import java.util.*;

/** In-memory cache of labels that takes care of enforcing constraints
 * (each label is in only 1 set; 1 song cannot have multiple labels from a set; no cycles).
 * Data is first loaded when this object is created.
 * Do not modify any returned objects; instead, call methods on this class to modify them.
 * Do not provide models as arguments unless those objects were previously returned by methods of this class. (Don't create your own models.)
 * TODO document how returned objects may change & how to handle this (listeners?)*/
public class Labels
{
	private final Orm orm = Storage.orm();
	private Map<Long, LabelModel> labels;
	private Map<Long, SetModel> sets;


	public Labels(Database db)
	{
		//labels
		List<SongLabel> labelRecords = orm.selectAll(SongLabel.class, db);
		labels = new HashMap<>(labelRecords.size());
		for(SongLabel record : labelRecords)
			labels.put(record.id, LabelModel.from(record));

		//parents
		for(Label2Label record : orm.selectAll(Label2Label.class, db))
		{
			LabelModel parent = labels.get(record.parent), child = labels.get(record.child);
			parent.children.add(child);
			child.parents.add(parent);
		}

		//sets
		List<LabelComparableSet> setRecords = orm.selectAll(LabelComparableSet.class, db);
		sets = new HashMap<>(setRecords.size());
		for(LabelComparableSet record : setRecords)
			sets.put(record.id, new SetModel(record.id, record.name, record.description));

		//labels in sets
		List<Label2Set> label2Set = orm.selectAll(Label2Set.class, db);
		label2Set.sort(Comparator.comparingInt(x -> x.index));//so when we add labels to the per-set lists, they get added in the right order
		for(Label2Set l2s : label2Set)
			sets.get(l2s.set).labels.add(labels.get(l2s.label));
	}


	public synchronized Collection<LabelModel> labels()
	{
		return Collections.unmodifiableCollection(labels.values());
	}

	public synchronized Collection<SetModel> sets()
	{
		return Collections.unmodifiableCollection(sets.values());
	}

	public synchronized LabelModel getLabel(long id){return labels.get(id);}
	public synchronized SetModel getSet(long id){return sets.get(id);}



	public synchronized LabelModel createLabel(SongLabel record, Database db)
	{
		db.beginTransaction();
		orm.insert(record, db);
		LabelModel label = LabelModel.from(record);
		labels.put(label.id, label);
		db.commit();
		return label;
	}

	public synchronized void update(LabelModel label, String name, String description, Database db)
	{
		assert label.id != -1;
		assert labels.get(label.id) == label;
		SongLabel record = new SongLabel(name, description);
		orm.save(record, db);
		label.name = name;
		label.description = description;
	}

	public synchronized void deleteLabel(LabelModel label, Database db)
	{
		db.beginTransaction();
		labels.forEach((id, other) ->
		{
			other.children.remove(label);
			other.parents.remove(label);
		});
		labels.remove(label.id);
		sets.forEach((id, set) -> set.labels.remove(label));
		db.commit();
	}


	public synchronized void addParentTo(LabelModel parent, LabelModel child, Database db)
	{
		assert parent.children.contains(child) == child.parents.contains(parent);
		if(parent.children.contains(child))
			return;

		Set<LabelModel> newParents = allIndirectParents(parent);
		if(newParents.contains(child))
			throw new RuntimeException(String.format("Refusing to make %s a parent of %s because this would result in a parent-child cycle.", parent, child));
		Label2Label record = new Label2Label(parent.id, child.id);
		orm.insert(record, db);
		parent.children.add(child);
		child.parents.add(parent);
	}


	public synchronized void removeParentFrom(LabelModel parent, LabelModel child, Database db)
	{
		Label2Label record = orm.createQuery(Label2Label.class)
				.andWhereEquals("parent", parent.id)
				.andWhereEquals("child", child.id)
				.executeFirst(db);
		orm.delete(record, db);
		child.parents.remove(parent);
		parent.children.remove(child);
	}



	private static Set<LabelModel> allIndirectParents(LabelModel label)
	{
		Set<LabelModel> out = new HashSet<>(label.parents);
		while(true)
		{
			int prevSize = out.size();
			for(LabelModel oldParent : out)
				out.addAll(oldParent.parents);
			if(out.size() == prevSize)
				return out;
		}
	}
}
