package imp.music.storage;

import imp.music.data.*;
import imp.music.data.relations.Label2Label;
import imp.music.data.relations.Label2Set;
import imp.orm.Orm;
import imp.sqlite.Database;
import imp.sqlite.main.Sqlite;
import impatience.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**Deals with finding and setting up the storage dir & database.*/
public class Storage
{
	private static final Logger log = LogManager.getLogger(Storage.class);
	private static final Path DATA_REDIRECT_FILE = Paths.get("data-location");
	private static final Path DATA_DIR;
	static
	{
		if(Files.exists(DATA_REDIRECT_FILE))
		{
			String redirect = FileUtil.read(DATA_REDIRECT_FILE);
			log.info("Redirecting to data dir path {}", redirect);
			DATA_DIR = Paths.get(redirect);
		}
		else
			DATA_DIR = Paths.get("data");
		log.info("Data dir is {}", DATA_DIR);
	}

	private static final Path DB = Paths.get(DATA_DIR.toString(), "music_data.sqlite");
	private static final Path FILES_DIR = Paths.get(DATA_DIR.toString(), "files");
	
	private static final Class[] TABLES = {Song.class, SongFile.class, SongLink.class, SongLabel.class, Label2Label.class, Label2Set.class, LabelComparableSet.class};


	/**Returns whether or not the database exists.*/
	private static boolean isSetup(){return Files.exists(DB);}
	
	
	/**Creates a connection to the database.*/
	public static Database connect()
	{
		return Sqlite.file(DB.toString());
	}
	
	
	/**Sets up an ORM object for interacting with the database.*/
	public static Orm orm()
	{
		return new Orm(TABLES);
	}


	/**Returns the path to an audio file (which might not exist).*/
	public static Path audioFile(long fileId, String fileExtension){return Paths.get(FILES_DIR.toString(), fileId + "." + fileExtension);}
	

	/**Sets up all tables in the database unless it already exists.*/
	public static void setupIfNecessary()
	{
		if(isSetup())
			return;
		
		try
		{
			Files.createDirectories(DATA_DIR);
			Files.createDirectories(FILES_DIR);
		} catch(IOException e){throw new RuntimeException(e);}
		
		try(Database db = connect())
		{
			orm().initialize(db);
		} catch(Throwable e) {
			try{Files.deleteIfExists(DB);}catch(IOException e2){/*ignore*/}
			throw e;
		}
	}
}
