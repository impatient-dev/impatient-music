package imp.music.data;

public class Song extends DbTable
{
	public String name, artist, mix, description;


	public Song(){}
	public Song(long id, String name, String artist, String mix, String description)
	{
		this.id = id;
		this.name = name;
		this.artist = artist;
		this.mix = mix;
		this.description = description;
	}


	@Override public String toString(){return id + ": " + name + " - " + artist + " (" + mix + ")";}
	
	/**Returns a user-friendly string.*/
	public String toFriendlyString()
	{
		String out = name + " - " + artist;
		if(!mix.isEmpty())
			out += " (" + mix + ")";
		return out;
	}
}
