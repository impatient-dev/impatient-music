package imp.music.data.relations;

import imp.music.data.DbTable;
import imp.music.data.LabelComparableSet;
import imp.music.data.SongLabel;
import imp.orm.markup.References;

/** Marks a label as being part of a set.
 * Currently, you should only allow a label to be in 1 set at a time. */
public class Label2Set extends DbTable
{
	@References(SongLabel.class) public long label;
	@References(LabelComparableSet.class) public long set;
	/**Used to sort labels in a set.*/
	public int index;
}
