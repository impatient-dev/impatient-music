package imp.music.data.relations;

import imp.music.data.DbTable;
import imp.music.data.SongLabel;
import imp.orm.markup.References;

/** Groups labels into a hierarchy. A child label IMPLIES its parent;
 * any song with a child label is considered to also have all parents applied, recursively.*/
public class Label2Label extends DbTable
{
	@References(SongLabel.class) public long parent, child;

	private Label2Label(){}

	public Label2Label(long parent, long child)
	{
		this.parent = parent;
		this.child = child;
	}
}
