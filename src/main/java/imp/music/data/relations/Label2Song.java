package imp.music.data.relations;

import imp.music.data.DbTable;
import imp.music.data.Song;
import imp.music.data.SongLabel;
import imp.orm.Orm;
import imp.orm.markup.References;

public class Label2Song extends DbTable
{
	@References(SongLabel.class) public long label;
	@References(Song.class) public long song;

	private Label2Song(){this(Orm.INVALID_KEY, Orm.INVALID_KEY);}

	public Label2Song(long label, long song)
	{
		this.label = label;
		this.song = song;
	}
}
