package imp.music.data;

/** Used to group songs. */
public class SongLabel extends DbTable
{
	public String name, description;

	private SongLabel(){}

	public SongLabel(String name, String description)
	{
		this.name = name;
		this.description = description;
	}
}
