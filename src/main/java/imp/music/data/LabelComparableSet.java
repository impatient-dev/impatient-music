package imp.music.data;

/** A set of labels. A song can only be labeled (directly or indirectly) with 1 of the labels in a set. */
public class LabelComparableSet extends DbTable
{
	public String name, description;
}
