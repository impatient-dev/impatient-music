package imp.music.data;

import imp.orm.markup.References;

/**A link to a song on e.g. SoundCloud.*/
public class SongLink extends DbTable
{
	@References(Song.class) public long song;
	public String url, description;
}
