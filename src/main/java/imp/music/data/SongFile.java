package imp.music.data;

import imp.music.storage.Storage;
import imp.orm.Orm;
import imp.orm.markup.References;
import imp.orm.markup.TableLifecycle;
import imp.sqlite.Database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**An audio file for a song.
 * A record exists in this table if and only if a matching file exists in our files directory.
 * Currently only one of these can exist per song.*/
public class SongFile extends DbTable implements TableLifecycle
{
	@References(Song.class) public long song;
	public String fileExtension, description;


	private SongFile(){}
	private SongFile(long song, String fileExtension, String description)
	{
		this.song = song;
		this.fileExtension = fileExtension;
		this.description = description;
	}


	public SongFile copy()
	{
		return new SongFile(song, fileExtension, description);
	}


	@Override public String toString(){return song + "." + fileExtension;}
	
	@Override public void postDelete()//TODO test
	{
		try{Files.deleteIfExists(Storage.audioFile(song, fileExtension));}
		catch(IOException e){throw new RuntimeException(e);}
	}


	/**Inserts and returns a new SongFile record, and also copies the file into our storage directory.*/
	public static SongFile importFile(Path file, Song song, Database db, Orm orm)
	{
		String name = file.getFileName().toString();
		int period = name.indexOf('.');
		if(period == -1 || period + 1 == name.length() || name.substring(period + 1, name.length()).indexOf('.') != -1)
			throw new IllegalArgumentException("Unsure what the file extension is for " + file);
		String extension = name.substring(period + 1, name.length());

		db.beginTransaction();
		SongFile record = new SongFile(song.id, extension, "");
		orm.insert(record, db);
		try{Files.copy(file, Storage.audioFile(record.id, extension));}
		catch(IOException e){throw new RuntimeException("Failed to copy song file", e);}

		db.commit();
		return record;
	}
}
