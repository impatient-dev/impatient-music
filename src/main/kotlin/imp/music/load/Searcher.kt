package imp.music.load

import imp.util.logger
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread
import kotlin.concurrent.withLock


private val log = Searcher::class.logger


//TODO make this generic and separate out JavaFX

/**TODO*/
class Searcher <In, Out> (
	val threadName: String,
	/**If requireIdle is false, a search will run at most this long after a touch() happens.
	 * If requireIdle is true, a search will run once there has not been any touch() for this long.*/
	val delayMs: Long = 300,
	val requireIdle: Boolean = false,
	/**Called in the JavaFX UI thread to load user input.*/
	val getInput: () -> In,
	/**Called in a background thread to generate a result for the user input.*/
	val search: (In) -> Out,
	/**Called in the JavaFX UI thread to post the result of the search.*/
	val consumeResult: (Out) -> Unit
) {
	@Volatile private var stop = false
	/**The last time we were TODO*/
	@Volatile private var lastTouchMs: Long
	private val sync = Object()
	private val lock: Lock = ReentrantLock()
	private val condition = lock.newCondition() //TODO replace with synchronized/notifyAll unless you find a reason not to


	init {
		val nowMs = System.currentTimeMillis()
		lastTouchMs = nowMs
		thread(isDaemon = true, name = threadName) {run(nowMs)} //TODO uncaught exception handler
	}


	/**Notifies this thread that the user input has changed, so we should run another search sometime.*/
	fun touch() {
		lastTouchMs = System.currentTimeMillis()
		lock.withLock {condition.signalAll()}
		TODO()
	}

	/**Stops the thread.*/
	fun close() {
		log.debug("Stopping searcher thread {}.", threadName)
		stop = true
		lock.withLock {
			log.trace("Signalling stop of thread {}.", threadName)
			condition.signalAll()
		}
	}


	private fun run(startMs: Long) {
		var lastSearchMs = startMs
		try {
			while (!stop) {
				lock.lock()
				val currentMs = lastTouchMs
				if(currentMs == lastTouchMs) {


				}

					log.debug("Running search: currently {}ms out of date.", currentMs - lastSearchMs)

				condition.await()
				lock.unlock()

				TODO()
			}
		} finally {
			log.debug("Thread stopping.")
		}
	}
}


/*
TODO requirements
-wait until idle
-wait until delay after first input
-
 */