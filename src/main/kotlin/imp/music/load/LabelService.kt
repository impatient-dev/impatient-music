package imp.music.load

import imp.music.data.*
import imp.orm.kt.Table
import imp.sqlite.Database
import imp.util.CompareUtil
import imp.util.logger
import java.lang.Integer.max

/**Saves/loads labels. "Save" means "insert or update".
 * The model object you provide to a function may have its ID set (if the record is new and being inserted).*/
object LabelService {
	private val log = LabelService::class.logger


	fun getAll(db: Database, store: Datastore): ArrayList<LabelModel> {
		log.debug("Retrieving all labels.")
		val dbLabels = musicOrm.query(Label::class).selectAll(db)
		val out = ArrayList<LabelModel>(dbLabels.size)
		val byId = HashMap<Long, LabelModel>()

		for(record in dbLabels) {
			val model = LabelModel.from(record, store)
			out.add(model)
			byId[model.id] = model
		}

		val relations = musicOrm.query(Label2Label::class).selectAll(db)
		for(relation in relations) {
			val parent = byId[relation.parentId]!!
			val child = byId[relation.childId]!!
			parent.children.add(child)
			child.parents.add(parent)
		}

		return out
	}


	fun save(label: LabelModel, db: Database) {
		log.debug("Saving {}.", label)
		db.beginTransaction()
		val record = label.toData()
		musicOrm.table(Label::class).save(record, db)
		label.id = record.id

		val index: (Int) -> Int? = if(label.isSorted) {i: Int -> i} else {i: Int -> null}

		val desiredParentRecords = ArrayList<Label2Label>(label.parents.size)
		label.parents.forEachIndexed {i, parent -> desiredParentRecords.add(Label2Label(parentId = parent.id, childId = label.id, idx = index(i)))}
		val desiredChildRecords = ArrayList<Label2Label>(label.children.size)
		label.children.forEachIndexed {i, child -> desiredChildRecords.add(Label2Label(parentId = label.id, childId = child.id, idx = index(i)))}

		val tableLabelRel = musicOrm.table(Label2Label::class)
		val parentRecords: MutableList<Label2Label> = musicOrm.query(tableLabelRel)
			.andWhere("childId = ?").param(label.id)
			.selectAll(db)
		val childRecords: MutableList<Label2Label> = musicOrm.query(tableLabelRel)
			.andWhere("parentId = ?").param(label.id)
			.selectAll(db)

		desiredParentRecords.sortWith(myLabelRelComparator)
		desiredChildRecords.sortWith(myLabelRelComparator)
		parentRecords.sortWith(myLabelRelComparator)
		childRecords.sortWith(myLabelRelComparator)

		log.debug("Updating from {} parents to {}.", parentRecords.size, desiredParentRecords.size)
		reconcileIgnoringExpectedIds(desiredParentRecords, parentRecords, db, tableLabelRel)
		log.debug("Updating from {} parents to {}.", childRecords.size, desiredParentRecords.size)
		reconcileIgnoringExpectedIds(desiredChildRecords, childRecords, db, tableLabelRel)

		db.commit()
		log.debug("Successfully saved {}", label)
	}


	/**Inserts, updates, and/or deletes records so that the actual list becomes the desired list.
	 * Ignores the IDs of the desired records.
	 * Both lists should be sorted so desired records line up with the actual ones; otherwise, this function may perform excessive updates.
	 * The provided objects may have their IDs updated.*/
	private fun reconcileIgnoringExpectedIds(desired: List<Label2Label>, actual: List<Label2Label>, db: Database, table: Table<Label2Label>) {
		for(i in 0 until max(desired.size, actual.size)) {
			val d = if(i >= desired.size) null else desired[i]
			val a = if(i >= actual.size) null else actual[i]
			if(d == null) {
				log.debug("Deleting {}.", a)
				table.delete(a!!, db)
			} else if(a == null) {
				log.debug("Inserting {}.", d)
				table.insert(d, db)
			} else if(d.childId != a.childId || d.parentId != a.parentId || d.idx != a.idx) {
				log.debug("Updating {} to {}.", a, d)
				d.id = a.id
				table.save(d, db)
			}
		}
	}


	fun delete(label: LabelModel, db: Database) {
		log.debug("Deleting {}.", label)
		if(label.id == -1L)
			throw Exception("Cannot delete non-existent label: $label")
		musicOrm.table(Label::class).deletePk(label.id, db)
	}
}


private fun myCompare(a: Label2Label, b: Label2Label): Int {
	if(a.idx != b.idx)
		return CompareUtil.nullsLess(a.idx, b.idx)
	if(a.parentId != b.parentId)
		return a.parentId.compareTo(b.parentId)
	return a.childId.compareTo(b.childId)
}

private val myLabelRelComparator: Comparator<Label2Label> = Comparator {a,b -> myCompare(a,b)}