package imp.music.load

import imp.music.data.LabelModel

/**Contains thread pools where all background work should be performed.*/
object AppBackground {
	val labelSearch = AppThreadPool<List<LabelModel>>("labelSearch", 1, 100, 60)
}