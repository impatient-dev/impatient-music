package imp.music.load

import imp.util.logger
import impatience.Threading
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicLong

private val log = AppThreadPool::class.logger

/**A customized version of ThreadPoolExecutor where all threads stop if they don't receive work for long enough,
 * and the names of dead threads are recycled.
 * The code that generates thread names is not particularly performant; don't use this with a huge number of threads that start and stop often.
 * TODO add listeners so the UI can tell when loading is happening.*/
class AppThreadPool <T: Any> (
	/**Base name of all threads. "foo" will produce threads like "foo-1" and "foo-2"*/
	private val name: String,
	maxThreads: Int,
	/**An exception will be thrown if you try to submit more tasks when this many are in the queue.*/
	private val maxBacklog: Int,
	/**Threads will stop if they have no work to do for this long (combined with threadTimeoutUnit).*/
	private val threadTimeout: Long = 10,
	private val threadTimeoutUnit: TimeUnit = TimeUnit.SECONDS
) {
	private val backlog = AtomicLong(0)
	private val exec = DecrementingThreadPoolExecutor(maxThreads, threadTimeout, threadTimeoutUnit, backlog)

	init {
		require(maxThreads < 1_000_000) {"maxThreads is too high: $maxThreads."}
	}

	fun submit(task: Callable<T>): Future<T> {
		val currentBacklog = backlog.incrementAndGet()
		log.debug("Submitting task to thread pool $name; backlog $currentBacklog.")
		if(currentBacklog > maxBacklog) {
			backlog.decrementAndGet()
			throw IllegalStateException("Backlog of tasks is too large ($backlog waiting; max is $maxBacklog")
		}
		return exec.submit(task)
	}

	fun shutdown() {
		log.debug("Shutting down thread pool $name.")
		exec.shutdown()
	}
}

/**Decrements an atomic int each time a task starts. Tightly coupled to ImpThreadPool.*/
private class DecrementingThreadPoolExecutor (
	maxThreads: Int,
	threadTimeout: Long,
	threadTimeoutUnit: TimeUnit,
	private val taskCounterToDecrement: AtomicLong
) : ThreadPoolExecutor(maxThreads, maxThreads, threadTimeout, threadTimeoutUnit, LinkedBlockingQueue()) {

	init {
		allowCoreThreadTimeOut(true)
	}

	override fun beforeExecute(thread: Thread?, task: Runnable) {
		taskCounterToDecrement.decrementAndGet()
	}
}


/**Generates thread names, reusing indexes from threads that have stopped. Uses a single lock for all operations.*/
private class IndexReusingThreadNameFactory (
	/**Every thread's name will start with this, followed by a number.*/
	private val baseName: String,
	/**Maximum number of threads that can be alive at a time. Errors will occur if this is exceeded.*/
	val maxThreads: Int
) : ThreadFactory {
	private val group = ThreadGroup(baseName)
	private val threadsTaken = Array<Thread?>(maxThreads) {null}
	private var nextIndexToTake = 0

	@Synchronized override fun newThread(r: Runnable): Thread {
		for(i in nextIndexToTake until nextIndexToTake + maxThreads * 2) {

			//if we've looked at every index once, release our lock and give other threads a chance to indicate they've stopped
			if(i == nextIndexToTake + maxThreads)
				Threading.wait(this, 1000)

			val index = i % maxThreads
			val existing = threadsTaken[i]
			if(existing == null || !existing.isAlive) {
				val out = Thread(group, r, "$baseName-${index+1}")
				threadsTaken[index] = out
				nextIndexToTake = (index + 1) % maxThreads
				log.debug("Creating thread ${out.name}.")
				return out;
			}
		}

		throw Exception("Failed to find a thread index to reuse.")
	}
}