package imp.music

import imp.log4j2.ImpLog4j2Init
import imp.music.data.AppConfig
import imp.music.data.loadAppConfig
import imp.music.data.musicOrm
import imp.music.ui.MusicWindow
import imp.util.RootDirFinder
import imp.util.logger
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage
import java.nio.file.Paths
import kotlin.system.exitProcess


lateinit var appConfig: AppConfig

fun main(args: Array<String>) {
	//initialize logging (avoid calling any methods that might use logging before this point)
	val appDir = RootDirFinder.find("imp-music.jar", "src/main/kotlin", logging = false)
	val logFile = Paths.get(appDir.toString(), "log4j2.xml")
	ImpLog4j2Init.initWithFile(logFile, "/log4j2-default.xml", MusicJavaFxApp::class)

	val log = MusicJavaFxApp::class.logger
	log.info("App starting up; logging initialized.")

	try {
		musicOrm.toString() //throws ExceptionInInitializerError if the ORM can't initialize
		appConfig = loadAppConfig(appDir)
		log.debug("Launching UI.")
		Application.launch(MusicJavaFxApp::class.java)
	} catch(e: Throwable) {
		log.fatal("Shutting down due to unexpected error.", e)
		Thread.sleep(500)
		exitProcess(1)
	}
}



class MusicJavaFxApp : Application() {
	private val log = MusicJavaFxApp::class.logger

	override fun start(primaryStage: Stage) {
		try {
			log.debug("Starting imp-music JavaFX application.")
			primaryStage.title = "impatient-music"
			primaryStage.scene = Scene(MusicWindow().root)
			primaryStage.show()
			log.debug("App JavaFX startup finished successfully.")
		} catch(e: Exception) {
			log.fatal("UI startup failed.", e)
			Thread.sleep(500)
			exitProcess(1)
		}
	}
}