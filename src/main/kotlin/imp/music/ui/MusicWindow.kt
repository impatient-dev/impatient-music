package imp.music.ui

import imp.music.data.LabelModel
import javafx.scene.Parent
import javafx.scene.layout.BorderPane

/**Main window of this application.*/
class MusicWindow {
	private val menu = MainMenuUi(this)
	private val search = LabelSearchUi()
	private val rightPane = RightPaneUi(this)
	val root: Parent = BorderPane(search.root, menu.root, rightPane.root, null, null)


	fun openLabel(label: LabelModel, newTab: Boolean) {
		val ui = if(newTab)
			LabelEditUi(label).also {rightPane.tabs.tabs.add(it.tab)}
		else
			TODO()
		ui.requestFocus()//TODO doesn't work
	}
}