package imp.music.ui

import javafx.collections.ListChangeListener
import javafx.scene.Parent
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.StackPane

/**Contains methods for interacting with a tab pane.
 * Each tab lets the user edit a label or song.*/
class RightPaneUi (window: MusicWindow) {
	private val vEmpty = EmptyRightPaneUi(window)
	val tabs = TabPane().also {it.tabs.addListener(ListChangeListener {change -> onTabListChange(change)})}
	private val stackPane = StackPane(vEmpty.root)
	val root: Parent = stackPane


	/**Swaps between the tab pane and a "nothing to see" empty component depending on whether there are any tabs.*/
	private fun onTabListChange(change: ListChangeListener.Change<out Tab>) {
		while(change.next()) {
			if (change.wasAdded() && change.addedSize == tabs.tabs.size) {
				stackPane.children.clear()
				stackPane.children.add(tabs)
			} else if (change.wasRemoved() && change.list.isEmpty()) {
				stackPane.children.clear()
				stackPane.children.add(vEmpty.root)
			}
		}
	}
}