package imp.music.ui

import imp.music.data.LabelModel
import imp.music.data.LabelSearch
import imp.music.load.AppBackground
import impatience.JfxUtil
import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.layout.VBox
import javafx.scene.text.Text

/**Pane that lets the user search for labels, and shows a results table.*/
class LabelSearchUi {
	private val colName = JfxUtil.tableCol<LabelModel, String>("Name") {it.name}
	private val colChildren = JfxUtil.tableCol<LabelModel, Int>("Children") {it.children.size}

	private val colType = JfxUtil.tableCol<LabelModel, String>("Type") {
		if(it.isSet) {
			if(it.isSorted) "Sorted Set"
			else "Set"
		} else ""
	}


	private val vTitle = Text("Label Search")
	private val vSearch = TextField().also {it.promptText = "Search for Labels"}.also {
		it.onAction = EventHandler {submitSearch()}
	}
	private val vResultsTitle = Text("Label Search Results(?)")//TODO
	private val vResults = TableView<LabelModel>().also {it.columns.addAll(colName, colChildren, colType)}

	val root: Parent = VBox(vTitle, vSearch, vResultsTitle, vResults)




	private fun submitSearch() {
		AppBackground.labelSearch.submit(LabelSearch("", jfxLater = { vResults.items.setAll(it)}) )
	}
}