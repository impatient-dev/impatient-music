package imp.music.ui

import imp.music.data.LabelModel
import javafx.geometry.Orientation
import javafx.scene.Parent
import javafx.scene.control.ContextMenu
import javafx.scene.control.TextField
import javafx.scene.control.TitledPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.VBox

/**TODO*/
class LabelListUi (val list: MutableList<LabelModel>, title: String) {
	private val vFlow = FlowPane(Orientation.HORIZONTAL)

	private val vEntry = TextField().also {it.promptText = "type a label"}
	private val vAutocomplete = ContextMenu()

//	private val vEntry = ComboBox<String>().also {
//	it.promptText = "type a label"
//	it.isEditable = true
//	it.placeholder = Text("no matching labels")
//}

	private val vContent = VBox(vFlow, vEntry)
	val root: Parent = TitledPane(title, vContent)
}