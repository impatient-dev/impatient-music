package imp.music.ui

import imp.music.appConfig
import imp.music.data.LabelModel
import imp.music.load.LabelService
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.*
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.text.Text

/**Lets the user edit a label (including a new label that we're creating).*/
class LabelEditUi (val label: LabelModel) {
	private val vTitle = Text("Label")
	private val vSave = Button("Save").also {it.onAction = EventHandler {save()}} //TODO replace with automatic saving if user closes tab or stops typing
	private val vClone = Button("Clone").also {it.isDisable = true}
	private val vDelete = Button("Delete").also {it.onAction = EventHandler {deleteAndClose()}}
	private val vStatusRow = HBox(vSave, vClone, vDelete).also {it.alignment = Pos.CENTER_RIGHT}

	private val vName = TextField().also {it.promptText = "Name"}
	private val vDescription = TextArea().also {it.promptText = "Description"}


	private val setGroup = ToggleGroup()
	private val vNotSet = RadioButton("Normal").also {it.toggleGroup = setGroup}
	private val vSet = RadioButton("Set").also {it.toggleGroup = setGroup}
	private val vSortedSet = RadioButton("Sorted Set").also {it.toggleGroup = setGroup}
	private val vSetRow = HBox(vNotSet, vSet, vSortedSet)

	private val vParents = LabelListUi(label.parents, "Parents")
	private val vChildren = LabelListUi(label.children, "Children")

	private val root: Parent = VBox(vTitle, vStatusRow, vName, vDescription, vSetRow, vParents.root, vChildren.root)
	val tab = Tab("", root)

	init {
		updateUi()
	}


	fun requestFocus() = vName.requestFocus()


	private fun updateUi() {
		tab.text = label.name
		vName.text = label.name
		vDescription.text = label.description
		val setBtnToEnable: RadioButton = if(label.isSorted) vSortedSet else if(label.isSet) vSet else vNotSet
		setBtnToEnable.isSelected = true
	}

	private fun updateModel() {
		label.name = vName.text
		label.description = vDescription.text
		label.isSet = !vNotSet.isSelected
		label.isSorted = vSortedSet.isSelected
	}


	private fun save() {
		updateModel()
		if(label.store == null)
			label.store = appConfig.datastores[0]
		label.store!!.useDb {db -> LabelService.save(label, db)}
		updateUi()
	}

	private fun deleteAndClose() {
		//TODO delete asynchrously and reopen the tab if it fails
		if(label.id != -1L) {
			label.store!!.connectDb().use { db ->
				LabelService.delete(label, db)
			}
		}
		tab.tabPane.tabs.remove(tab)
	}
}
