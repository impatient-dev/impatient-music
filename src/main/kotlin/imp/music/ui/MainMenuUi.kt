package imp.music.ui

import imp.music.data.LabelModel
import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem

/**A menu bar with some options.*/
class MainMenuUi (val window: MusicWindow) {
	private val vNewSong = MenuItem("New Song").also {it.isDisable = true}
	private val vNewLabel = MenuItem("New Label").also {it.onAction = EventHandler {window.openLabel(LabelModel.newUnsaved(), true)} }
	private val vMenu = Menu("Menu", null, vNewSong, vNewLabel)
	val root: Parent = MenuBar(vMenu)
}