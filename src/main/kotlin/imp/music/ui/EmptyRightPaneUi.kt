package imp.music.ui

import imp.music.data.LabelModel
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.layout.VBox
import javafx.scene.text.Text

/**Shown when there are no tabs in the right pane with songs/labels to view.*/
class EmptyRightPaneUi (window: MusicWindow) {
	private val vText = Text("There's nothing here.")
	private val vNewSong = Button("New Song").also {it.isDisable = true}
	private val vNewLabel = Button("New Label").also {it.onAction = EventHandler {window.openLabel(LabelModel.newUnsaved(), true)} }
	val root: Parent = VBox(vText, vNewSong, vNewLabel).also {it.alignment = Pos.CENTER}
}