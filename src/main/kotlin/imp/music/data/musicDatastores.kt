package imp.music.data

import imp.sqlite.Database
import imp.sqlite.main.Sqlite
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


/**A place where songs and their metadata can be stored.
 * Each datastore has a separate database, and a separate audio directory.
 * Each datastore is an immutable singleton.*/
class Datastore (
	val path: Path
) {
	override fun toString() = "Datastore($path)"
	val dbFile = Paths.get(path.toString(), "data.sqlite")
	fun connectDb(): Database = Sqlite.file(dbFile.toString())
	val audioDir = Paths.get(path.toString(), "audio")
	inline fun useDb(block: (Database) -> Unit) = connectDb().use {block(it)}
}

class AppConfig(
	/**There is always at least 1.*/
	val datastores: List<Datastore>
) {
	init {
		require(!datastores.isEmpty())
	}
}


private val log = Datastore::class.logger

fun loadAppConfig(appDir: Path): AppConfig {
	log.info("Loading app config.")
	val datastoresFile = Paths.get(appDir.toString(), "datastores.txt")
	val datastores = ArrayList<Datastore>()

	log.debug("Searching for datastore locations in {}", datastoresFile)
	if(Files.exists(datastoresFile)) {
		log.debug("Found datastores file {}", datastoresFile)
		val lines = Files.readAllLines(datastoresFile)
		for(line in lines) {
			datastores.add(validateAndInitDatastore(line.trim()))
		}
	}

	if(datastores.isEmpty()) {
		val defaultDatastore = Paths.get(appDir.toString(), "data")
		log.info("No datastores found. Adding default datastore: {}", defaultDatastore)
		Files.createDirectories(defaultDatastore)
		datastores.add(validateAndInitDatastore(defaultDatastore.toString()))
	}

	return AppConfig(datastores)
}


private fun validateAndInitDatastore(pathStr: String): Datastore {
	log.debug("Validating datastore {}", pathStr)
	val path = parsePath(pathStr)
	if(path == null)
		throw Exception("Invalid datastore path: $path")
	if(!Files.isDirectory(path))
		throw Exception("Must be a real directory: $path")

	val out = Datastore(path)
	if(!Files.exists(out.dbFile)) {
		log.info("Initializing missing database {}", out.dbFile)
		try {
			out.connectDb().use {db ->
				musicOrm.init(db)
			}
		} catch(e: Exception) {
			try {
				log.debug("Failed to initialize DB; deleting {}", out.dbFile)
				Files.deleteIfExists(out.dbFile)
			} catch(e2: Exception) {
				log.error("Failed to delete db ${out.dbFile} after failed init.", e2)
			}
			throw Exception("Failed to initialize $out", e)
		}
	}

	return out
}

private fun parsePath(str: String): Path? {
	try {
		return Paths.get(str)
	} catch(e: Exception) {
		log.error("Failed to parse path: {}", str)
		throw Exception("Invalid path: $str")
	}
}