package imp.music.data

import imp.music.appConfig
import imp.music.load.LabelService
import javafx.application.Platform
import java.util.concurrent.Callable

/**Finds all labels that match a search.*/
class LabelSearch(
	val str: String,
	/**When the search is done, uses the result on the JavaFX application thread.*/
	private val jfxLater: ((List<LabelModel>) -> Unit)? = null
) : Callable<List<LabelModel>> {
	override fun call(): ArrayList<LabelModel> {
		val out = ArrayList<LabelModel>()
		appConfig.datastores.forEach {store ->
			store.connectDb().use {db ->
				for(model in LabelService.getAll(db, store)) {
					if(str.isBlank() || model.name.contains(str) || model.description.contains(str))
						out.add(model)
				}

			}
		}

		if(jfxLater != null)
			Platform.runLater { jfxLater!!(out) }

		return out
	}
}