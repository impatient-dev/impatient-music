package imp.music.data

import imp.orm.kt.Orm
import imp.orm.kt.PrimaryKey
import imp.orm.kt.References
import java.util.*


val musicOrm = Orm(listOf(
	Song::class,
	Label::class,
	SavedSearch::class,
	Label2Label::class,
	Label2Song::class,
	Label2SongFile::class
))


data class Song (
	@PrimaryKey var id: Long = -1,
	var uuid: String = UUID.randomUUID().toString(),
	var name: String = "",
	var artist: String = "",
	var mix: String = "",
	var description: String = ""
) {
	override fun toString() = "Song($id $uuid)"
}

/**Every song has 0 or 1 files. Support for multiple files (e.g. different quality levels) may be added in the future.*/
data class SongFile (
	@PrimaryKey var id: Long = -1,
	var uuid: String = UUID.randomUUID().toString(),
	@References("Song") val songId: Long,
	var description: String = "",
	/**Relative to the datasource base directory.*/
	var relativePath: String
) {
	override fun toString() = "SongFile(song $songId, file $id $uuid)"
}

data class Label (
	@PrimaryKey var id: Long = -1,
	var uuid: String = UUID.randomUUID().toString(),
	var name: String = "",
	var description: String = "",
	var isSet: Boolean = false,
	var isSorted: Boolean = false
) {
	override fun toString() = "Label($id $name)"
}

data class SavedSearch (
	@PrimaryKey var id: Long = -1,
	var uuid: String = UUID.randomUUID().toString(),
	var name: String,
	var search: String
) {
	override fun toString() = "Search($id)"
}



//relations

data class Label2Label (
	@PrimaryKey var id: Long = -1,
	@References("Label") var parentId: Long,
	@References("Label") var childId: Long,
	/**Can be used to sort child labels if the parent is an exclusive set.*/
	var idx: Int?
) {
	override fun toString() = "Label2Label($childId is child #$idx of $parentId)"
}

data class Label2Song (
	@PrimaryKey var id: Long = -1,
	@References("Song") var songId: Long,
	@References("Label") var labelId: Long
) {
	override fun toString() = "label $labelId, song $songId"
}

data class Label2SongFile (
	@PrimaryKey var id: Long = -1,
	@References("SongFile") var songFileId: Long,
	@References("Label") var labelId: Long
) {
	override fun toString() = "label $labelId, song file $songFileId"
}