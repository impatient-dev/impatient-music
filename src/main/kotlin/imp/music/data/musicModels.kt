package imp.music.data

import java.nio.file.Path
import java.util.*
import kotlin.collections.ArrayList


class SongModel (
	var store: Datastore,
	var id: Long,
	var uuid: UUID,
	var name: String,
	var artist: String,
	var mix: String,
	var description: String,
	val labels: List<LabelModel>,
	val files: List<SongFile>
) {
	override fun toString() = "SongM($id $uuid)"
	fun toData() = Song(id, uuid.toString(), name, artist, mix, description)
}


class SongFileModel (
	var store: Datastore,
	var id: Long,
	var uuid: UUID,
	val song: SongModel,
	var description: String,
	/**Relative to the root of the datastore files dir.*/
	var relativePath: Path
) {
	override fun toString() = "SongFileM($id $uuid for $song)"
	fun toData() = SongFile(id, uuid.toString(), song.id, description, relativePath.toString())
}


/**Equality is by ID and datastore.*/
class LabelModel private constructor (
	/**Null if it hasn't been chosen yet.*/
	var store: Datastore?,
	var id: Long,
	var uuid: UUID,
	var name: String,
	var description: String,
	val parents: ArrayList<LabelModel> = ArrayList(),
	val children: ArrayList<LabelModel> = ArrayList(),
	var isSet: Boolean,
	var isSorted: Boolean
) {
	override fun toString() = "LabelM($id $uuid $name)"
	fun toData() = Label(id, uuid.toString(), name, description, isSet)

	override fun hashCode() = id.hashCode()
	override fun equals(that: Any?) = that is LabelModel && that.id == this.id && that.store == this.store

	companion object {
		fun from(record: Label, store: Datastore) = LabelModel(store, record.id, UUID.fromString(record.uuid), record.name, record.description,
			isSet = record.isSet, isSorted = record.isSorted)

		fun newUnsaved() = LabelModel(null, -1, UUID.randomUUID(), "", "", isSet = false, isSorted = false)
	}
}
