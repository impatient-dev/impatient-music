package imp.music.storage;

import imp.music.data.SongLabel;
import imp.music.model.LabelModel;
import imp.orm.Orm;
import imp.sqlite.Database;
import imp.sqlite.main.Sqlite;
import impatience.CollectionUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LabelsTest
{
	private Orm orm = Storage.orm();

	private Database testDb()
	{
		Database out = Sqlite.inMemory();
		orm.initialize(out);
		return out;
	}

	@Test public void hierarchy()
	{
		try(Database db = testDb())
		{
			Labels l = new Labels(db);
			SongLabel a = new SongLabel("a", ""), b = new SongLabel("b", ""), c = new SongLabel("c", "");
			l.createLabel(a, db);
			l.createLabel(b, db);
			l.createLabel(c, db);

			LabelModel aa = l.getLabel(a.id), bb = l.getLabel(b.id), cc = l.getLabel(c.id);

			l.addParentTo(aa, bb, db);
			assertEquals(aa, CollectionUtil.getOnly(bb.parents));
			assertEquals(bb, CollectionUtil.getOnly(aa.children));

			l.addParentTo(aa, cc, db);
			assertEquals(2, aa.children.size());

			l.removeParentFrom(aa, bb, db);
			assertEquals(cc, CollectionUtil.getOnly(aa.children));
			assertEquals(aa, CollectionUtil.getOnly(cc.parents));
			assertTrue(bb.parents.isEmpty());

		}
	}


	@Test public void saving()
	{
		try(Database db = testDb())
		{
			Labels l = new Labels(db);
			l.createLabel(new SongLabel("name", ""), db);
			Labels l2 = new Labels(db);
			assertEquals(CollectionUtil.getOnly(l.labels()), CollectionUtil.getOnly(l2.labels()));
		}
	}
}